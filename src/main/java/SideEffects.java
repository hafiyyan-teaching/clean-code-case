import java.nio.charset.Charset;
import java.util.Date;
import java.util.Random;

public class SideEffects {

    public static void main(String[] args){

        User tempUser = new User();

        //Session Service
        SessionService1 service1 = new SessionService1();
        System.out.println(service1.login(tempUser, "advprog"));

    }
}

class User {

    private Session session;

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public boolean passwordMatched(String password) {
        return password.contains("advprog");
    }

    @Override
    public String toString() {
        return "User{" +
                "session=" + session +
                '}';
    }
}

class Session {

    private Date lastActivity;
    private String token;

    public Session(){
        this.lastActivity = new Date();
        this.token = "";
    }

    public Date getLastActivity() {
        return lastActivity;
    }

    public void setLastActivity(Date lastActivity) {
        this.lastActivity = lastActivity;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "Session{" +
                "lastActivity=" + lastActivity +
                ", token='" + token + '\'' +
                '}';
    }
}

//with side effects
class SessionService1 {

    public Session login(User user, String password) {
        if (user.getSession() != null) {
            return user.getSession();
        } else if (user.passwordMatched(password)) {
            Session session = new Session();

            byte[] array = new byte[7]; // length is bounded by 7
            new Random().nextBytes(array);
            String generatedString = new String(array, Charset.forName("UTF-8"));
            session.setToken(generatedString);

            user.setSession(session);
            return session;
        }
        return null;
    }
}


//after refactor with CQRS
class SessionService2 {

    public void userPerformLogin(User user, String password){
        if(!isAlreadyLoggedIn(user)){
            getAuthenticatedUser(user, password);
        }
    }

    private boolean isAlreadyLoggedIn(User user) {
        return user.getSession() != null;
    }

    private void getAuthenticatedUser(User user, String password) {
        if (user.passwordMatched(password)) {
            user.setSession(generateSessionForLoggedInUser());
        }
    }

    private String generateToken(){
        byte[] array = new byte[7]; // length is bounded by 7
        new Random().nextBytes(array);
        String generatedString = new String(array, Charset.forName("UTF-8"));

        return generatedString;
    }

    private Session generateSessionForLoggedInUser(){
        Session generatedSession = new Session();
        generatedSession.setToken(generateToken());

        return generatedSession;
    }
}