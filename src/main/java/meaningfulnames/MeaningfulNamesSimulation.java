package meaningfulnames;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MeaningfulNamesSimulation {
    public static void main(String[] args){

        //Containing Current Date
        String yyyymmdstr = new SimpleDateFormat("YYYY/MM/DD").format(new Date());

    }


    //Make meaningful name that can be distinguished
    public static void copyChars(char[] a1, char[] a2){
        for(int i = 0; i < a1.length; i++){
            a2[i] = a1[i];
        }
    }

}

//Don't add unneeded context in your variable name
class Car {
    public String carMake = "Honda";
    public String carModel = "Accord";
    public String carColor = "Blue";
}


//Do not use a name that can not be pronounced
class Customer{

    private Date genymdhms; //Generation Timestamp
    private Date modymdhms; //Modification Timestamp
    private final String pszqint = "102"; //Record ID
}